


/* ------------- CONSTANTS ------------- */

var pixelsPerSecond = 150;
var measureOffColor = "rgb(255, 255, 255)";
var measureOnColor = "rgb(128, 128, 128)";
var spaceOffColor = "rgba(0, 0, 0, 0)";
var spaceOnColor = "rgb(165, 42, 42)";




/* ------------- GLOBAL VARIABLES ------------- */

var totalSecondsLeft = 8.0;
var notesPerOctave = 12;
var secondsInMeasure = 2.0;

var countsPerMeasure = 4;



/* ------------- SIMPLISTIC HELPER FUNCTIONS ------------- */

function textifyColor(elt)
{
	var style = window.getComputedStyle(elt,"");
	var color = style.getPropertyValue("background-color");
	var colorText = "" + color;
	
	return colorText;
}
function numeralifyWidth(elt)
{
	var style = window.getComputedStyle(elt,"");
	var width = style.getPropertyValue("width");
	var numericalWidth = parseInt(newWidth);
	
	return numericalWidth;
}
function lowerMeasureBorder(textBorder)
{
	var numericalBorder = parseInt(textBorder);
	
	if(numericalBorder == 10) {numericalBorder = 7;}
	else if(numericalBorder == 7) {numericalBorder = 4.5;}
	else if(numericalBorder == 4.5) {numericalBorder = 2.5;}
	else if(numericalBorder == 2.5) {numericalBorder = 1;}
	
	return numericalBorder;
}




/* ------------- FUNCTIONS FOR SELECTING DIVS AND TOGGLING COLORS ------------- */

function selectBox(box)
{
	
	
	boxClass = box.className;
	
	if(boxClass == "measureController")
	{
		toggleMeasureColor(box);
	}
	else if(boxClass == "space")
	{
		toggleSpaceColor(box);
	}
}
function toggleMeasureColor(measure)
{
	var color = textifyColor(measure);
	
	if(color == measureOffColor)
	{
		measure.style.backgroundColor = measureOnColor;
	}
	else if(color == measureOnColor)
	{
		measure.style.backgroundColor = measureOffColor;
	}
}
function toggleSpaceColor(space)
{
	var color = textifyColor(space);

	if(color == spaceOffColor || color == "transparent") // i have to have both - chrome sees the color as an rgba with all 0's, while firefox just calls it "transparent".
	{
		space.style.backgroundColor = spaceOnColor;
	}
	else if(color == spaceOnColor)
	{
		space.style.backgroundColor = spaceOffColor;
	}
	
	document.getElementById("disp").innerHTML = color;
}













/* ------------- FUNCTIONS FOR PROCESSING NUMBER INPUTS (AND UPDATING RELATED DESCRIPTIONS) ------------- */

function changeNotesPerOctave()
{
	var n = document.getElementById("notesPerOctave").value;
	
	notesPerOctave = n;
    updateMeasureDescription();
}
function changeSecondsInMeasure()
{
	var n = document.getElementById("secondsInMeasure").value;
	
	secondsInMeasure = n;
    updateMeasureDescription();
}
function updateMeasureDescription()
{
	var newMD = "Create a new Measure with "+notesPerOctave+" notes per octave that is "+secondsInMeasure+" seconds long.";
	document.getElementById("measureDescription").innerHTML = newMD;
}

function changeCountsPerMeasure()
{
	var n = document.getElementById("countsPerMeasure").value;
	
	countsPerMeasure = n;
    updateCountDescription();
}
function updateCountDescription()
{
	var newCD = "Divide this measure into "+countsPerMeasure+" counts.";
	document.getElementById("countDescription").innerHTML = newCD;
}










/* ------------- THE "CREATE NEW MEASURE" FUNCTION (AND RELATED "UPDATE DESCRIPTION" FUNCTIONS) ------------- */

function createNewMeasure()
{
	if(secondsInMeasure > totalSecondsLeft)
	{
		alert("You don't have enough seconds left for a measure that is "+secondsInMeasure+" seconds long. Please choose another number for your measure lenght that is no greater than "+totalSecondsLeft+" seconds long.");
		return;
	}
	
	var measureStartTime = (8000 - (totalSecondsLeft*1000))/1000;
	var theNewMeasure = createMeasure(notesPerOctave, secondsInMeasure, measureStartTime, 10);
	
	document.getElementById("musicContainer").appendChild(theNewMeasure);
	
	updateTotalSecondsLeft();
}
function updateTotalSecondsLeft()
{
	var tempTSL = totalSecondsLeft * 1000;
	var tempSIM = secondsInMeasure * 1000;
	var tempDiff = tempTSL - tempSIM;
	tempDiff /= 1000;
	
	totalSecondsLeft = tempDiff;
	
	var newSLD = "You have "+totalSecondsLeft+" seconds available.";
	document.getElementById("secondsLeftDescription").innerHTML = newSLD;
}


















/* ------------- INTERNAL FUNCTIONS FOR CREATING MEASURES ------------- */

function createMeasure(npo, sim, startTime, borderWidth) //npo = notes per octave, sim = seconds in measure
{
	var pixelsInMeasure = sim*pixelsPerSecond - borderWidth;
	var pixelsInMeasureText = "" + pixelsInMeasure + "px";

	var newMeasure = document.createElement("div");
	newMeasure.style.width = pixelsInMeasureText;
	newMeasure.className = "measure";
	newMeasure.style.borderWidth = borderWidth + "px";
	
	var theNewOct4 = createOctave("oct4", npo);
	var theNewOct3 = createOctave("oct3", npo);
	var theNewOct2 = createOctave("oct2", npo);
	var theNewOct1 = createOctave("oct1", npo);
	var theNewMeasureController = createMeasureController((sim*1000), (startTime*1000), borderWidth);
	
	newMeasure.appendChild(theNewOct4);
	newMeasure.appendChild(theNewOct3);
	newMeasure.appendChild(theNewOct2);
	newMeasure.appendChild(theNewOct1);
	newMeasure.appendChild(theNewMeasureController);
	
	return newMeasure;
}
function createOctave(octaveLevel, numOfNotes)
{
	var octave = document.createElement("div");
	octave.className = "octave " + octaveLevel;
	
	for(var i = 0; i < numOfNotes; i++)
	{
		var spaceHeight = 120 / numOfNotes;
		spaceHeight--;
		var spaceHeightText = "" + spaceHeight + "px";
		
		var newSpace = document.createElement("div");
		newSpace.style.height = spaceHeightText;
		newSpace.className = "space";
		
		
		newSpace.addEventListener("click", function(){ selectBox(this); })
		//newSpace.addEventListener("click", function(){ toggleSpaceColor(this); });
		
		//newSpace.style.cursor = "pointer";
		//newSpace.style.zIndex = "10";
		
		var spaceNumber = numOfNotes-1-i;
		var octaveNumber = octaveLevel.substring(3, 4);
		newSpace.setAttribute("data-octave", octaveLevel);
		newSpace.setAttribute("data-notes", numOfNotes);
		newSpace.setAttribute("data-note", spaceNumber);
		if(spaceNumber == 0){ newSpace.title = "note is: a" + octaveNumber; }
		else { newSpace.title = "note is: a" + octaveNumber + " + " + spaceNumber + "/" + numOfNotes + " of an octave"; }
		
		octave.appendChild(newSpace);
	}
	
	return octave;
}
function createMeasureController(dataLength, dataStart, dataBorder)
{
	var newMeasureController = document.createElement("div");
	newMeasureController.className = "measureController";
	newMeasureController.addEventListener("click", function(){ selectBox(this); });
	
	newMeasureController.setAttribute("data-length", dataLength);
	newMeasureController.setAttribute("data-start", dataStart);
	newMeasureController.setAttribute("data-border", dataBorder);
	
	//dataLength = Math.round(dataLength);
	newMeasureController.title = "measure is " + (dataLength/1000) + " seconds long, and starts at " + (dataStart/1000) + " seconds in";
	
	return newMeasureController;
}








/* ------------- THE "DIVIDE" FUNCTION ------------- */

function divideMeasure()
{
	measureControllers = document.getElementsByClassName("measureController");
	
	for(var i = 0; i < measureControllers.length; i++)
	{
		var measureController = measureControllers[i];
		var color = textifyColor(measureController);
		
		if(color == measureOnColor)
		{
			divideThisMeasure(measureController);
		}
	}
}
function divideThisMeasure(measureController)
{
	var dataLength = measureController.getAttribute("data-length");
	var dataStart = measureController.getAttribute("data-start");
	var dataBorder = measureController.getAttribute("data-border");

	var measure = measureController.parentElement;
	var followingMeasure = measure.nextSibling;

	var firstOctave = measure.firstChild;
	var firstSpace = firstOctave.firstChild;
	var numOfNotes = firstSpace.getAttribute("data-notes");
	
	var newBorder = lowerMeasureBorder(dataBorder);
	var startTimeInMiliseconds = parseInt(dataStart);
	
	var lengthInMiliseconds = parseInt(dataLength);
	var newLengthInMiliseconds = lengthInMiliseconds/countsPerMeasure;
	var roundedNewLengthInMiliseconds = Math.round(newLengthInMiliseconds);
	var lengthInSeconds = roundedNewLengthInMiliseconds/1000;
	
	for(var i = 0; i < (countsPerMeasure - 1); i++)
	{
		var newDataStart = (startTimeInMiliseconds + (roundedNewLengthInMiliseconds*(i+1)))/1000;
		var newCount = createMeasure(numOfNotes, lengthInSeconds, newDataStart, newBorder);
		
		if(i == (countsPerMeasure - 2))
		{
			newCount.style.width = ((lengthInSeconds*150) - dataBorder) + "px";
			newCount.style.borderWidth = dataBorder + "px";
		}
	
		document.getElementById("musicContainer").insertBefore(newCount,followingMeasure);
	}

	var adjWidth = (lengthInSeconds*150) - newBorder;
	measure.style.width = adjWidth + "px";
	measure.style.borderWidth = newBorder + "px";
	
	measure.removeChild(measureController);
	
	var newMC = createMeasureController(roundedNewLengthInMiliseconds, startTimeInMiliseconds, newBorder);
	measure.appendChild(newMC);
}






/* ------------- THE "COMBINE" FUNCTION ------------- */

function combineMeasures()
{
	var indexForFirstSelectedMeasure = 0;
	var totalMiliseconds = 0;
	var firstSelectedMeasure = true;
	var container = document.getElementById("musicContainer");
	
	selectedMeasures = document.getElementsByClassName("measureController");
	var arraySize = selectedMeasures.length;
	
	for(var i = 0; i < selectedMeasures.length; i++)
	{
		var measureController = measureControllers[i];
		var color = textifyColor(measureController);
		if(color == measureOnColor)
		{
			indexForFirstSelectedMeasure = i;
			measureController.style.backgroundColor = "white";
			firstSelectedMeasure = false;
			
			var dataLength = measureController.getAttribute("data-length");
			numericalDataLength = parseInt(dataLength);
			totalMiliseconds += numericalDataLength;
			
			break;
		}
	}
	
	for (var j = selectedMeasures.length-1; j > indexForFirstSelectedMeasure; j--)
	{
		var measureController = measureControllers[j];
		var color = textifyColor(measureController);
		if(color == measureOnColor)
		{
			var dataLength = measureController.getAttribute("data-length");
			numericalDataLength = parseInt(dataLength);
			totalMiliseconds += numericalDataLength;
			
			removeMeasure(measureController);
		}
	}
	
	if(!firstSelectedMeasure)
	{
		var measureController = measureControllers[indexForFirstSelectedMeasure];
		var measure = measureController.parentElement;
		
		var dataStart = measureController.getAttribute("data-start");
		var dataBorder = measureController.getAttribute("data-border");
		var borderSize = parseInt(dataBorder);
		
		measure.style.width = ((totalMiliseconds/1000*150) - borderSize) + "px";

		measure.removeChild(measureController);
	
		var newMC = createMeasureController(totalMiliseconds, dataStart, dataBorder);
		measure.appendChild(newMC);
	}
	

	
	/*
	for (var k = indexForFirstSelectedMeasure+1; k < selectedMeasures.length; k++)
	{
		var measureController = measureControllers[k];
		updateMeasure(measureController);
	}
	*/
}

function removeMeasure(measureController)
{
	var measure = measureController.parentElement;
	var container = document.getElementById("musicContainer");
	container.removeChild(measure);
}
function updateMeasure(measureController)
{
	
}






/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */
/* ------------- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ---- S O U N D ------------- */










/* ------------- SETTING UP THE AUDIO WEB API'S AND THE OSCILLATOR ------------- */





var vol = 0.1;

var context = new AudioContext();            // CREATES AUDIO CONTAINER/CONTEXT
	o = context.createOscillator();          // CREATES OSCILLATOR (HERE CALLED JUST "O"., WHICH I LIKE...)
	theGain = context.createGain();          // CREATES SOMETHING ANALOGOUS TO A PEDAL THAT THE OSC RUNS THROUGH. WE USE IT HERE TO CONTROL THE VOLUME, BUT I THINK YOU CAN USE IT TO MODIFY THE SOUND IN LOTS OF OTHER WAYS TOO.

	o.type = "square";
	o.frequency.value = 100;
	theGain.gain.value = 0;                  // THE GAIN (PEDAL) IS SET TO ZERO VOLUME. THIS IS WHY THE WEBSITE IS SILENT UPON LOADING.

	o.start(0);                              // THIS STARTS THE OSC INSTANTLY (THE ZERO MEANS "IN ZERO SECONDS")
	o.connect(theGain);                      // THIS RUNS THE OSC THROUGH THE GAIN (PEDAL).
	theGain.connect(context.destination);    // AND THIS CONNECTS THE GAIN TO WHATEVER IS INSIDE THE WEBSITE THAT ACTUALLY ALLOWS US TO HEAR IT.




/* ------------- CONTROLLING VOLUME ------------- */

function makeAudible(time)
{
	theGain.gain.value = vol;
	
	document.getElementById("sec").innerHTML = time;
}
function makeSilent(time)
{
	theGain.gain.value = 0;
	
	document.getElementById("freq").innerHTML = "SILENT";
	document.getElementById("sec").innerHTML = time;
}



/* ------------- CONTROLLING FREQUENCY ------------- */

function changeFreq(freq)
{
	o.frequency.value = freq;
	document.getElementById("freq").innerHTML = freq + " Hz";
	
	/*
	var theTime = context.currentTime;
	theTime *= 100;
	theTime = Math.round(theTime);
	theTime /= 100;
	var timeStr = "" + theTime + " : " + freq + "<br/>";
	document.getElementById("time").innerHTML += timeStr;
	*/
}










/* ------------- PLAYING THE SONG ------------- */


function playSong()
{
	

	var allMeasures = document.getElementsByClassName("measure"); 
	//var end = bars.length * 250;
	//setTimeout(makeSilent, end);
	
	
	//var newReadOut = "";
	
	
	
	for(var i = 0; i < allMeasures.length; i++)
	{
		
		
		
		var measure = allMeasures[i];
		var spacesInMeasure = measure.getElementsByClassName("space");
		var measureControllers = measure.getElementsByClassName("measureController");
		var measureController = measureControllers[0];
		
		var dataLength = measureController.getAttribute("data-length");
		var dataStart = measureController.getAttribute("data-start");
		
		var lengthOfMeasureInMiliseconds = parseInt(dataLength);
		var startOfMeasureInMiliseconds = parseInt(dataStart);
		
		//newReadOut += thisBar.id + " " + items.length;
		
		
		
		var noRed = true;
		
		
		
		for(var j = 0; j < spacesInMeasure.length; j++)
		{
			
			
			var thisSpace = spacesInMeasure[j];
			var color = textifyColor(thisSpace);
				
			if (color == spaceOnColor)
			{
				
				
				var dataOctave = thisSpace.getAttribute("data-octave");
				var dataNotes = thisSpace.getAttribute("data-notes");
				var dataNote = thisSpace.getAttribute("data-note");

				var octaveNumber = dataOctave.substring(3, 4);
				var notesInOctave = parseInt(dataNotes);
				var noteThatIsThisSpace = parseInt(dataNote);
				
				
				
				var octaveFrequency = 55*(Math.pow(2,(octaveNumber-1)));
				var oneNoteIncrease = Math.pow(2,(1.0 / notesInOctave));
				var multiplier = Math.pow(oneNoteIncrease,noteThatIsThisSpace);
				var freq = octaveFrequency * multiplier;
					
				setTimeout(changeFreq, startOfMeasureInMiliseconds, freq);
				setTimeout(makeAudible, startOfMeasureInMiliseconds, startOfMeasureInMiliseconds);
				
				
				var noRed = false;
				
				break;
				
			}
		
			
		}
		
		if(noRed)
		{
			setTimeout(makeSilent, startOfMeasureInMiliseconds, startOfMeasureInMiliseconds);
			//newReadOut += " " + "NONE" + "<br/>";
		}
	}
	
	var lengthOfSong = 8000 - (totalSecondsLeft*1000);
	setTimeout(makeSilent, lengthOfSong);
	
	
	//document.getElementById("display").innerHTML = newReadOut;
}








